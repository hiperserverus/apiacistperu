'use strict'

var express = require('express');
var app = express();
var usuarioController = require('../controllers/usuario');

app.get('/usuario', usuarioController.obtenerUsuarios);

module.exports = app;