'use strict'

var express = require('express');
var app = express();
var obraController = require('../controllers/obra');

app.get('/obra', obraController.obtenerObras);

module.exports = app;