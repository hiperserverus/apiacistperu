'use strict'

var express = require('express');
var app = express();
var ubicacionController = require('../controllers/ubicacion');

app.get('/ubicacion', ubicacionController.obtenerUbicaciones);

module.exports = app;