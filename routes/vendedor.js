'use strict'

var express = require('express');
var app = express();
var vendedorController = require('../controllers/vendedor');

app.get('/vendedor', vendedorController.obtenerVendedores);

module.exports = app;