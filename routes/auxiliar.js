'use strict'

var express = require('express');
var app = express();
var auxiliarController = require('../controllers/auxiliar');

app.get('/auxiliar', auxiliarController.obtenerAuxiliares);

module.exports = app;