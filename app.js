var express = require('express');
var dao = require('./dao');
var bodyParser = require('body-parser');

// Inicializar variables
var app = express();
var methodOverride = require('method-override');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Importar Rutas
var appRoutes = require('./routes/app');
var auxiliaresRoutes = require('./routes/auxiliar');
var obrasRoutes = require('./routes/obra');
var usuariosRoutes = require('./routes/usuario');
var ubicacionesRoutes = require('./routes/ubicacion');
var vendedoresRoutes = require('./routes/vendedor');

// Rutas

app.use('/', appRoutes);
app.use('/', auxiliaresRoutes);
app.use('/', obrasRoutes);
app.use('/', usuariosRoutes);
app.use('/', ubicacionesRoutes);
app.use('/', vendedoresRoutes);


// Escuchar peticiones
var _PORT = app.set('port', process.env.PORT || 3000);
app.listen(3000, () => {
    console.log('Express server corriendo en el puerto 3000: \x1b[32m%s\x1b[0m', 'online');
});